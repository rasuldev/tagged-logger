import 'package:logger/logger.dart';
import 'package:tlogger/src/logger_config.dart';

class TLogger {
  final logger = LoggerConfig.getLogger();
  final String defaultTag;

  /// You can use class name as default tag
  TLogger({required this.defaultTag});

  void info(
    dynamic message, {
    String? tag,
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _log(Level.info, message,
        tag: tag, time: time, error: error, stackTrace: stackTrace);
  }

  void warn(
    dynamic message, {
    String? tag,
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _log(Level.warning, message,
        tag: tag, time: time, error: error, stackTrace: stackTrace);
  }

  void error(
    dynamic message, {
    String? tag,
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    _log(Level.error, message,
        tag: tag, time: time, error: error, stackTrace: stackTrace);
  }

  void _log(
    Level level,
    dynamic message, {
    String? tag,
    DateTime? time,
    Object? error,
    StackTrace? stackTrace,
  }) {
    logger.log(level, {"tag": tag ?? defaultTag, "message": message},
        time: time, error: error, stackTrace: stackTrace);
  }
}
