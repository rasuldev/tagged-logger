import 'package:logger/logger.dart';

class TaggedPrinter extends LogPrinter {
  final LogPrinter _realPrinter;

  TaggedPrinter(this._realPrinter);

  @override
  List<String> log(LogEvent event) {
    final taggedMessage = event.message;
    final tag = taggedMessage['tag'];
    var realLogs = _realPrinter.log(LogEvent(
        event.level, taggedMessage['message'],
        time: event.time, error: event.error, stackTrace: event.stackTrace));
    return realLogs.map((s) => '|$tag| $s').toList();
  }
}
