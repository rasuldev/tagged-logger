import 'package:logger/logger.dart';
import 'package:tlogger/src/tagged_printer.dart';

class LoggerConfig {
  static late final Logger _logger;

  static void init({bool colors = false}) {
    Logger.level = Level.trace;
    Logger.defaultPrinter = () => PrefixPrinter(
          TaggedPrinter(PrettyPrinter(
            methodCount: 0,
            noBoxingByDefault: true,
            printEmojis: false,
            colors: colors,
          )),
          debug: "",
          trace: "",
          verbose: "",
          wtf: "",
          fatal: "",
          error: "ERROR",
          info: "INFO",
          warning: "WARN",
        );
    _logger = Logger();
  }

  static Logger getLogger() => _logger;
}
