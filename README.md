<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

Adds tags to logger from https://github.com/SourceHorizon/logger

## Getting started
Before using tagged logger run
```dart
LoggerConfig.init()
```
## Usage

```dart
class Demo {
  static final logger = TLogger(defaultTag: "Demo");
  
  void SomeMethod() {
    logger.info("Message");
    logger.warn("Message");
    logger.error("Message");
  }
}
```